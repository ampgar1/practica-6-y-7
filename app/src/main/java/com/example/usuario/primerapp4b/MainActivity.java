package com.example.usuario.primerapp4b;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.security.auth.login.LoginException;

public class MainActivity extends AppCompatActivity {
    Button buttonLogin, buttonGuardar, buttonBuscar, buttonParametro , buttonFragmento, buttonSensor, buttonVibrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadLogin.class);
                startActivity(intent);
            }
        });
        buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intenth = new Intent(MainActivity.this,
                        ActividadBuscar.class);
                startActivity(intenth);
            }
        });
        buttonGuardar = (Button) findViewById(R.id.buttonGuardar);
        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentj = new Intent(MainActivity.this,
                        ActividadRegistrar.class);
                startActivity(intentj);
            }
        });
        buttonParametro = (Button) findViewById(R.id.btnPasarParametro);
        buttonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadPasarParametro.class);
                startActivity(intent);
            }
        });
        buttonFragmento = (Button) findViewById(R.id.btnFragmento);
        buttonFragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadFragmentos.class);
                startActivity(intent);
            }
        });
        buttonSensor = (Button) findViewById(R.id.btnSensor);
        buttonSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });
        buttonVibrar = (Button) findViewById(R.id.btnVibracion);
        buttonVibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ActividadVibracion.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.opcionLogin:

                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_log);

                Button botonAuntenticar = (Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario = (EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = (EditText) dialogoLogin.findViewById(R.id.txtPassword);

                botonAuntenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, cajaUsuario.getText().toString()
                                + "" + cajaClave.getText().toString(),Toast.LENGTH_SHORT).show();
                    }
                });
                dialogoLogin.show();
                break;
        }
        switch (item.getItemId()) {
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
        }
        return true;
    }
}